package tab.com.au.codetest.di.module

import android.content.Context
import dagger.Module
import dagger.Provides
import tab.com.au.codetest.model.JsonSerializer
import tab.com.au.codetest.repository.RaceDataRepository
import tab.com.au.codetest.service.RacesServiceApi
import javax.inject.Singleton

@Module
class RepositoryModule {

    @Singleton
    @Provides
    fun providesRacesDataRepository(
        context: Context,
        racesServiceApi: RacesServiceApi,
        jsonSerializer: JsonSerializer
    ) = RaceDataRepository(context, racesServiceApi, jsonSerializer)
}