package tab.com.au.codetest.di.module

import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import tab.com.au.codetest.BuildConfig
import tab.com.au.codetest.constant.AppConstant
import java.util.concurrent.TimeUnit
import javax.inject.Singleton

@Module
class HttpClientModule{

    @Singleton
    @Provides
    fun providesOkHttpClient(): OkHttpClient {
        val loggingLevel =
            if (BuildConfig.DEBUG) HttpLoggingInterceptor.Level.BODY else HttpLoggingInterceptor.Level.NONE
        return OkHttpClient()
            .newBuilder()
            .addInterceptor(HttpLoggingInterceptor().setLevel(loggingLevel))
            .readTimeout(AppConstant.OK_HTTP_CLIENT_READ_TIMEOUT_SECONDS, TimeUnit.SECONDS)
            .connectTimeout(AppConstant.OK_HTTP_CLIENT_CONNECT_TIMEOUT_SECONDS, TimeUnit.SECONDS)
            .build()
    }
}