package tab.com.au.codetest.core

import dagger.android.AndroidInjector
import dagger.android.DaggerApplication
import dagger.android.HasFragmentInjector
import tab.com.au.codetest.BuildConfig
import tab.com.au.codetest.di.DaggerAppComponent
import timber.log.Timber

class TabApplication : DaggerApplication(), HasFragmentInjector {
    override fun applicationInjector(): AndroidInjector<out DaggerApplication> {
        return DaggerAppComponent.builder().application(this).build()
    }

    override fun onCreate() {
        super.onCreate()
        if (BuildConfig.DEBUG) {
            Timber.plant(Timber.DebugTree())
        } else {
            Timber.plant(object : Timber.Tree() {
                override fun log(priority: Int, tag: String?, message: String, t: Throwable?) {}
            })
        }
    }
}