package tab.com.au.codetest.di.module

import dagger.Module
import dagger.Provides
import tab.com.au.codetest.model.JsonSerializer
import javax.inject.Singleton

@Module
class ParserModule{

    @Singleton
    @Provides
    fun providesJsonSerializer() = JsonSerializer()
}