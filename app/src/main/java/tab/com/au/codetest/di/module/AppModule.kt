package tab.com.au.codetest.di.module

import dagger.Module
import dagger.Provides
import tab.com.au.codetest.core.TabAppSchedulers
import tab.com.au.codetest.core.TabApplication
import javax.inject.Singleton

@Module
class AppModule{

    @Singleton
    @Provides
    fun providesApplicationContext(tabApplication: TabApplication) = tabApplication.applicationContext

    @Singleton
    @Provides
    fun providesSchedulers() = TabAppSchedulers()
}