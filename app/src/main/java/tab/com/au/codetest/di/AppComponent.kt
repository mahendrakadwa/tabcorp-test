package tab.com.au.codetest.di

import dagger.BindsInstance
import dagger.Component
import dagger.android.AndroidInjectionModule
import dagger.android.AndroidInjector
import dagger.android.support.AndroidSupportInjectionModule
import tab.com.au.codetest.core.TabApplication
import tab.com.au.codetest.di.module.*
import tab.com.au.codetest.di.viewmodel.ViewModelModule
import javax.inject.Singleton

@Singleton
@Component(
    modules = [
        AndroidInjectionModule::class,
        AndroidSupportInjectionModule::class,
        AppModule::class,
        HttpClientModule::class,
        ParserModule::class,
        ServiceApiModule::class,
        UiComponentModule::class,
        ViewModelModule::class
    ]
)
interface AppComponent : AndroidInjector<TabApplication> {

    @Component.Builder
    interface Builder {
        @BindsInstance
        fun application(tabApplication: TabApplication): Builder

        fun build(): AppComponent
    }
}