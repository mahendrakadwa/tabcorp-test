package tab.com.au.codetest.di.module

import dagger.Module
import dagger.android.ContributesAndroidInjector
import tab.com.au.codetest.ui.activity.MainActivity

@Module
abstract class UiComponentModule {

    @ContributesAndroidInjector
    abstract fun bindMainActivity(): MainActivity
}