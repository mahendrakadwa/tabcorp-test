package tab.com.au.codetest.extension

/***
 * Converts string into Word Case String
 */
fun String.toCamelcase(): String {
    val listOfWords = this.toLowerCase().split(" ").toTypedArray()
    var resultantString: String = ""
    listOfWords.forEach {
        resultantString += " ${it.capitalize()}"
    }
    return resultantString
}