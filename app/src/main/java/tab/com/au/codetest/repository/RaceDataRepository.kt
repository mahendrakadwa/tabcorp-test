package tab.com.au.codetest.repository

import android.content.Context
import androidx.annotation.RawRes
import androidx.annotation.WorkerThread
import io.reactivex.Observable
import tab.com.au.codetest.R
import tab.com.au.codetest.model.JsonSerializer
import tab.com.au.codetest.model.Races
import tab.com.au.codetest.service.RacesServiceApi
import java.io.IOException
import java.util.*
import javax.inject.Inject

class RaceDataRepository @Inject constructor(val context: Context, val racesServiceApi: RacesServiceApi, val jsonSerializer: JsonSerializer) {

    fun loadRaces(locale: Locale): Observable<Races> {
        return if (locale.country.equals("en_AU", ignoreCase = true)) {
            racesServiceApi.getRaces()
        } else {
            Observable.fromArray(loadFromResource(R.raw.races))
        }
    }

    /**
     * loads a a list of races from a local resourcef file
     *
     * @param res
     * @param context
     * @return
     * @throws IOException
     */
    @WorkerThread
    private fun loadFromResource(@RawRes res: Int): Races {

        val input = context.resources.openRawResource(res)
        return jsonSerializer.read(input, Races::class.java)

    }
}
