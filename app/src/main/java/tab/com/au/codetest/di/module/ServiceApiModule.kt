package tab.com.au.codetest.di.module

import com.jakewharton.retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient
import retrofit2.Retrofit
import retrofit2.converter.jackson.JacksonConverterFactory
import tab.com.au.codetest.model.JsonSerializer
import tab.com.au.codetest.service.RacesServiceApi
import javax.inject.Singleton

@Module
class ServiceApiModule{

    @Singleton
    @Provides
    fun provideRacesServiceApi(jsonSerializer: JsonSerializer, okHttpClient: OkHttpClient): RacesServiceApi {
        return Retrofit.Builder()
            .baseUrl("https://pre-api.beta.tab.com.au/v1/")
            .addConverterFactory(
                JacksonConverterFactory.create(jsonSerializer.objectMapper))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .client(okHttpClient)
            .build()
            .create(RacesServiceApi::class.java)
    }
}