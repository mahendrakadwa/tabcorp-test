package tab.com.au.codetest.ui.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.ImageView
import android.widget.TextView
import androidx.core.content.ContextCompat
import androidx.recyclerview.widget.RecyclerView
import org.joda.time.DateTime
import org.joda.time.LocalTime
import tab.com.au.codetest.R
import tab.com.au.codetest.extension.toCamelcase
import tab.com.au.codetest.model.Race

class RaceListAdapter(val raceList: List<Race>) : RecyclerView.Adapter<RaceListAdapter.RaceListItemViewHolder>() {
    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RaceListItemViewHolder {
        val inflater = LayoutInflater.from(parent.context)
        val v = inflater.inflate(R.layout.list_item_race, parent, false)
        return RaceListItemViewHolder(v)
    }

    override fun getItemCount(): Int {
        return raceList?.size ?: 0
    }

    override fun onBindViewHolder(viewHolder: RaceListItemViewHolder, position: Int) {
        viewHolder.apply {

            raceList[position]?.let { race ->
                if (race.meeting?.raceType!!.raceTypeImageResource != -1) {
                    raceTypeImageView.setImageResource(race.meeting?.raceType!!.raceTypeImageResource)
                    raceTypeImageView.visibility = View.VISIBLE
                } else {
                    raceTypeImageView.visibility = View.GONE
                }
                raceNameAndNumberTextView.text = raceNameAndNumberTextView.context.getString(
                        R.string.race_name_and_number,
                        race.raceName.toCamelcase(),
                        race.number
                )

                if (isHighlightTime(race.raceStartTime)) {
                    raceStartTimeTextView.setTextColor(ContextCompat.getColor(raceStartTimeTextView.context, R.color.colorRed))
                } else {
                    raceStartTimeTextView.setTextColor(ContextCompat.getColor(raceStartTimeTextView.context, R.color.grey))
                }
                race.raceStartTime?.let {
                    raceStartTimeTextView.text = getHoursAndMinutesFromTime(it)
                }
            }
        }
    }

    /**
     * Format time to have 0 before minutes if minutes are < 10
     */
    private fun getHoursAndMinutesFromTime(startTime: DateTime): CharSequence? {
        val minutes = if (startTime.toLocalTime().minuteOfHour > 9) "${startTime.toLocalTime().minuteOfHour}" else "0${startTime.toLocalTime().minuteOfHour}"
        return "${startTime.toLocalTime().hourOfDay}:$minutes"
    }

    inner class RaceListItemViewHolder(mItemView: View) : RecyclerView.ViewHolder(mItemView) {
        internal val raceTypeImageView: ImageView = mItemView.findViewById(R.id.race_type_IV)
        internal val raceNameAndNumberTextView: TextView = mItemView.findViewById(R.id.race_name_and_number_TV)
        internal val raceStartTimeTextView: TextView = mItemView.findViewById(R.id.race_start_time_TV)
    }

    private fun isHighlightTime(raceStartTime: DateTime): Boolean {
        val raceLocalTime = LocalTime(raceStartTime.toLocalTime().hourOfDay, raceStartTime.toLocalTime().minuteOfHour)
        val timeDiffInMillis = raceLocalTime.millisOfDay - LocalTime.now().millisOfDay
        return (timeDiffInMillis > 0 && timeDiffInMillis < (1000 * 60 * 60))
    }
}
