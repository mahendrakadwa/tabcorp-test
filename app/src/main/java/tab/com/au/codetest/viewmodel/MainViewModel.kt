package tab.com.au.codetest.viewmodel

import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import io.reactivex.disposables.Disposable
import tab.com.au.codetest.core.TabAppSchedulers
import tab.com.au.codetest.model.Races
import tab.com.au.codetest.repository.RaceDataRepository
import timber.log.Timber
import java.util.*
import javax.inject.Inject

class MainViewModel @Inject constructor(
    private val raceDataRepository: RaceDataRepository,
    private val tabAppSchedulers: TabAppSchedulers
) :
    ViewModel() {

    private lateinit var disposable: Disposable
    val raceLiveDataMutableLiveData by lazy { MutableLiveData<Races>() }

    fun loadRacesList(locale: Locale) {
        disposable = raceDataRepository.loadRaces(locale)
            .subscribeOn(tabAppSchedulers.io())
            .observeOn(tabAppSchedulers.mainThread())
            .subscribe({ racesList ->
                raceLiveDataMutableLiveData.value = racesList
            }, {
                Timber.d(MainViewModel::class.simpleName, "Exception while getting races list...")
            })
    }

    override fun onCleared() {
        super.onCleared()
        if (!disposable.isDisposed) disposable.dispose()
    }
}