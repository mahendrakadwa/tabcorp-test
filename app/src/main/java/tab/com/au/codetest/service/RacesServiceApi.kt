package tab.com.au.codetest.service


import io.reactivex.Observable
import retrofit2.http.GET
import tab.com.au.codetest.model.Races

interface RacesServiceApi {

    @GET("tab-info-service/racing/next-to-go/races?jurisdiction=NSW")
    fun getRaces(): Observable<Races>
}
