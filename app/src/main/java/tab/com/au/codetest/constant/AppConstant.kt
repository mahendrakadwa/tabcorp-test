package tab.com.au.codetest.constant

object AppConstant {
    val OK_HTTP_CLIENT_CONNECT_TIMEOUT_SECONDS: Long = 30
    val OK_HTTP_CLIENT_READ_TIMEOUT_SECONDS: Long = 30
    val OPENWEATHERMAP_API_SERVICE_URL: String = "http://api.openweathermap.org/"

    val OPENWEATHERMAP_WEATHRE_ICON_BASE_URL: String = "http://openweathermap.org/img/w/"

    val SIMPLE_TIME_FORMAT = "HH:mm:ss a"
}