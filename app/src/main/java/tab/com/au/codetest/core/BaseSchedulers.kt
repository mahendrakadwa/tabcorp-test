package tab.com.au.codetest.core

import io.reactivex.Scheduler

interface BaseSchedulers {

    fun io(): Scheduler

    fun computation(): Scheduler

    fun newThread(): Scheduler

    fun mainThread(): Scheduler

    fun single(): Scheduler

    fun trampoline(): Scheduler
}