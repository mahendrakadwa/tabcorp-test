package tab.com.au.codetest.ui.activity

import android.os.Bundle
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.lifecycle.ViewModelProviders
import androidx.recyclerview.widget.DefaultItemAnimator
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import dagger.android.AndroidInjection
import kotlinx.android.synthetic.main.activity_main.*
import tab.com.au.codetest.R
import tab.com.au.codetest.di.viewmodel.ViewModelFactory
import tab.com.au.codetest.model.Race
import tab.com.au.codetest.ui.adapter.RaceListAdapter
import tab.com.au.codetest.viewmodel.MainViewModel
import java.util.*
import javax.inject.Inject

class MainActivity : AppCompatActivity() {

    @Inject
    lateinit var viewModelFactory: ViewModelFactory

    private lateinit var viewModel: MainViewModel

    override fun onCreate(savedInstanceState: Bundle?) {
        AndroidInjection.inject(this@MainActivity)
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        setupRecyclerView()
        setupViewModel()
    }

    private fun setupViewModel() {
        viewModel = ViewModelProviders.of(this@MainActivity, viewModelFactory).get(MainViewModel::class.java)
        viewModel.loadRacesList(Locale.getDefault())
        viewModel.raceLiveDataMutableLiveData.observe(this, androidx.lifecycle.Observer { races ->

            races?.races?.let {
                populateRaceList(it)
            }
        })
    }

    private fun setupRecyclerView() {
        val linearLayoutManager = LinearLayoutManager(this@MainActivity)
        val dividerItemDecoration = DividerItemDecoration(this@MainActivity, DividerItemDecoration.VERTICAL)
        dividerItemDecoration.setDrawable(ContextCompat.getDrawable(this@MainActivity, R.drawable.list_divider)!!)

        race_list_RecyclerView.apply {
            layoutManager = linearLayoutManager
            itemAnimator = DefaultItemAnimator()
            addItemDecoration(dividerItemDecoration)
        }
    }

    private fun populateRaceList(raceList: List<Race>) {
        race_list_RecyclerView.apply {
            adapter = RaceListAdapter(raceList)
        }
    }
}
