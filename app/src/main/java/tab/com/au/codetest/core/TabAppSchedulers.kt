package tab.com.au.codetest.core

import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers

/**
 * Wrapper around rx android Schedulers
 */
class TabAppSchedulers : BaseSchedulers {

    override fun io() = Schedulers.io()

    override fun computation() = Schedulers.computation()

    override fun newThread() = Schedulers.newThread()

    override fun mainThread() = AndroidSchedulers.mainThread()

    override fun single() = Schedulers.single()

    override fun trampoline() = Schedulers.trampoline()
}