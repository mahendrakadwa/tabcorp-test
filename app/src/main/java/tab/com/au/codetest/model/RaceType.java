package tab.com.au.codetest.model;

import com.fasterxml.jackson.annotation.JsonCreator;

import tab.com.au.codetest.R;

public enum RaceType {
    UNKNOWN("", ""),
    THOROUGHBRED("R", "Races"),
    GREYHOUND("G", "Greyhounds"),
    HARNESS("H", "Harness");

    private String mCode;

    private String mLabel;

    RaceType(String code, String label) {
        mCode = code;
        mLabel = label;
    }

    public String code() {
        return mCode;
    }

    @Override
    public String toString() {
        return mLabel;
    }

    @JsonCreator
    public static RaceType fromCode(String code) {
        if ("R".equalsIgnoreCase(code)) {
            return THOROUGHBRED;
        } else if ("G".equalsIgnoreCase(code)) {
            return GREYHOUND;
        } else if ("H".equalsIgnoreCase(code)) {
            return HARNESS;
        } else {
            return UNKNOWN;
        }
    }

    public int getRaceTypeImageResource() {
        if (this == THOROUGHBRED) return R.mipmap.ic_horse;
        if (this == GREYHOUND) return R.mipmap.ic_greyhound;
        if (this == HARNESS) return R.mipmap.ic_harness;
        else return -1;
    }
}
